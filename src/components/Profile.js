import React from 'react';
import PropTypes from 'prop-types';

const Profile = ({user}) => {
  function stroke() {
    // console.log('Ca fait clic');
    document.getElementById('mail').classList.add('mail--stroke');
  }

  Profile.propTypes = {
    user: PropTypes.string,
  };

  return (
    user.map((element, position) => (
      <div key={position} className="profile">
        <h2 className="profile__name">{element.name.first} {element.name.last}</h2>

        <img className="profile__image" src={element.picture.large}/>

        <div className="profile__userName">
          <span className="profile__info">Nom d'utilisateur: </span>
          {element.login.username}
        </div>

        <div className="profile__age">
          <span className="profile__info">Age: </span>
          {element.dob.age} ans
        </div>

        <div className="profile__email" onClick={stroke}>
          <span className="profile__info">Email: </span>
          <span className="mail" id="mail">{element.email}</span>
        </div>

        <div className="profile__phone">
          <span className="profile__info">Tél: </span>
          {element.phone}
        </div>
      </div>
    ))
  );
};

export default Profile;
