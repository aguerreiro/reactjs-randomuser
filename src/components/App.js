import React from 'react';
import Profile from './Profile';

const App = ({data}) => {
  return (
    <div className="dataUser">
      <Profile user={data.results}/>
    </div>
  );
};
export default App;
